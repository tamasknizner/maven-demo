package demo.api;

import org.apache.commons.lang3.SystemUtils;

import com.google.gson.Gson;

public interface MySuperApi {

    default void f() {
        SystemUtils.getHostName();
        new Gson();
    }

}
